# Portfolio de Chahrazed HATHAT

## Description
Ce portfolio a été créé dans le cadre de ma formation de développeur web et web mobile. Il s'agit d'un projet visant à appréhender les bases du HTML et du CSS, ainsi que l'utilisation de Bootstrap pour le développement web responsive. Le site offre la possibilité de découvrir mon parcours, mes projets annexes et de me contacter directement via un formulaire.

## Technologies Utilisées
- HTML
- CSS
- JavaScript
- Bootstrap

## Captures d'écran
![home](./assets/images/portfoliosc1.png)
![projets](./assets/images/portfoliosc2.png)
![skills](./assets/images/portfoliosc3.png)
![contact](./assets/images/portfoliosc4.png)

## Comment Utiliser
1. Cloner ce dépôt sur votre machine locale.
2. Ouvrir le fichier `index.html` dans votre navigateur web préféré pour visualiser le site.

## Maquette Figma
- [Lien vers la maquette Figma intiale](https://www.figma.com/file/wY8kDVPn5LdOe8IQYFz2XK/web-dev-portfolio?type=design&node-id=0%3A1&mode=design&t=z2R22CJZT66OrNrM-1)

---

N'hésitez pas à me contacter si vous avez des questions ou des commentaires !
